﻿using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace Helpers
{
    /// <summary>
    /// Functions for retrieving and processing media such as videos and images.
    /// </summary>
    public static class Media
    {
        /// <summary>
        /// Attempts to download a thumbnail for the specified video. Currently supports YouTube and Vimeo.
        /// </summary>
        /// <param name="videoURL">URL of YouTube or Vimeo video to retrieve the thumbnail for.</param>
        /// <param name="contentID">ID to use in the file name.</param>
        /// <param name="width">Width of the thumbnail image to save.</param>
        /// <param name="height">Height of the thumbnail image to save.</param>
        /// <param name="imageDirectory">Relative path to sub-directory in /images/content where the thumbnail should be saved.</param>
        public static void DownloadVideoThumbnail( string videoURL, int contentID, int width = 640, int height = 360, string imageDirectory = "videos" )
        {
            if( !String.IsNullOrWhiteSpace( videoURL ) )
            {
                Uri videoURI;

                // Validate the provided URL
                if( Uri.TryCreate( videoURL, UriKind.Absolute, out videoURI ) )
                {
                    ImagingProcess cropper = new ImagingProcess();

                    // Prepare the path to save the thumbnail to
                    string rawThumbPath = String.Format( "/images/content/{0}/{1}_{2}x{3}.jpg", imageDirectory.TrimStart( '/' ).TrimEnd( '/' ), contentID, width, height ).Replace( "//", "/" );
                    string thumbPath = HttpContext.Current.Server.MapPath( rawThumbPath );

                    // Prepare a temporary path to store the image before processing
                    string rawTempPath = rawThumbPath.Replace( width + "x" + height, "temp" );
                    string tempPath = thumbPath.Replace( width + "x" + height, "temp" );

                    bool thumbDownloaded = false;

                    using( WebClient connection = new WebClient() )
                    {
                        if( videoURL.Contains( "youtube.com" ) )
                        {
                            Match youtubeMatch = new Regex( "youtu(?:\\.be|be\\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)" ).Match( videoURL );

                            if( youtubeMatch.Success )
                            {
                                // Prepare the URL for the full-res thumbnail
                                string youtubeThumb = String.Format( "http://img.youtube.com/vi/{0}/maxresdefault.jpg", youtubeMatch.Groups[1].Value );

                                // Prepare the URL for the lower-res thumbnail as a fallback
                                string youtubeThumbSmall = String.Format( "http://img.youtube.com/vi/{0}/hqdefault.jpg", youtubeMatch.Groups[1].Value );

                                try
                                {
                                    // Attempt to download the full-res thumbnail
                                    connection.DownloadFile( youtubeThumb, tempPath );
                                    thumbDownloaded = true;
                                }
                                catch( Exception )
                                {
                                    // Fallback to crappy thumb
                                    connection.DownloadFile( youtubeThumbSmall, tempPath );
                                    thumbDownloaded = true;
                                }
                            }
                            else
                            {
                                // Invalid YouTube URL
                            }
                        }
                        else if( videoURL.Contains( "vimeo.com" ) )
                        {
                            Match vimeoMatch = new Regex( "vimeo\\.com/(?:.*#|.*/videos/)?([0-9]+)" ).Match( videoURL );

                            if( vimeoMatch.Success )
                            {
                                // Retrieve the thumbnail URL
                                string vimeoThumb = _getVimeoThumbnailURL( connection, vimeoMatch.Groups[1].Value );

                                // Download the thumbnail
                                if( !String.IsNullOrWhiteSpace( vimeoThumb ) )
                                {
                                    connection.DownloadFile( vimeoThumb, tempPath );
                                    thumbDownloaded = true;
                                }
                            }
                            else
                            {
                                // Invalid Vimeo URL
                            }
                        }
                        else
                        {
                            // Unsupported URL
                        }
                    }

                    if( thumbDownloaded )
                    {
                        using( Stream imageStream = File.OpenRead( tempPath ) )
                        {
                            cropper.CropResizeAndSave( imageStream, rawThumbPath, width, height );
                        }

                        File.Delete( tempPath );
                    }
                }
                else
                {
                    // Invalid URL
                }
            }
            else
            {
                // Missing URL
            }
        }

        #region Internal Functions

        /// <summary>
        /// Retrieves the URL for a Vimeo thumbnail.
        /// </summary>
        /// <param name="connection">Existing web connection to use for downloading the XML file.</param>
        /// <param name="videoID">Vimeo ID for the video to retrieve a thumbnail for.</param>
        private static string _getVimeoThumbnailURL( WebClient connection, String videoID )
        {
            // Get path to XML file containing the thumbnail URLs
            string xmlURL = String.Format( "http://vimeo.com/api/v2/video/{0}.xml", videoID );

            // Retrieve the XML file
            string rawXML = connection.DownloadString( xmlURL );

            // Parse XML as a document to traverse to find the thumbnails
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml( rawXML );

            // Attempt to retrieve the large thumbnail
            XmlNode thumbNode = xmlDoc.SelectSingleNode( "//video/thumbnail_large/text()" );

            // Fall back to medium thumbnail if large does not exist
            if( String.IsNullOrWhiteSpace( thumbNode.Value ) )
            {
                thumbNode = xmlDoc.SelectSingleNode( "//video/thumbnail_medium/text()" );
            }

            // Fall back to small thumbnail if medium does not exist
            if( String.IsNullOrWhiteSpace( thumbNode.Value ) )
            {
                thumbNode = xmlDoc.SelectSingleNode( "//video/thumbnail_small/text()" );
            }

            return thumbNode.Value;
        }

        #endregion
    }
}