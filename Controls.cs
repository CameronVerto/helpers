﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Helpers
{
    /// <summary>
    /// Functions for creating and interacting with controls.
    /// </summary>
    public static class Controls
    {
        /// <summary>
        /// Creates an <i> tag with the specified class to render an icon.
        /// </summary>
        /// <param name="cssClass">CSS class to add to the control.</param>
        /// <param name="hideARIA">Whether to apply aria-hidden to the control.</param>
        public static HtmlGenericControl CreateIcon( string cssClass, bool hideARIA = true )
        {
            HtmlGenericControl newIcon = new HtmlGenericControl( "i" );

            newIcon.Attributes.Add( "class", cssClass );
            newIcon.Attributes.Add( "aria-hidden", hideARIA.ToString().ToLower() );

            return newIcon;
        }

        /// <summary>
        /// Retrieves the nearest ancestor of the specified type.
        /// </summary>
        /// <param name="child">Control to begin search from.</param>
        public static T GetAncestor<T>( Control child )
        {
            // Prepare the type to return
            object returnControl = null;

            if( child.Parent != null && child.Parent is T )
            {
                returnControl = child.Parent;
            }
            else if( child.Parent != null )
            {
                returnControl = GetAncestor<T>( child.Parent );
            }

            return (T)returnControl;
        }

        /// <summary>
        /// Retrieves the nearest descendant of the specified type.
        /// </summary>
        /// <param name="parent">Control to begin search from.</param>
        public static T GetDescendant<T>( Control parent, string ID = null )
        {
            // Prepare the type to return
            object returnControl = null;

            foreach( Control child in parent.Controls )
            {
                if( child is T && ( ID == null || ID == child.ID ) )
                {
                    returnControl = child;
                }
                else returnControl = GetDescendant<T>( child );

                // Break the loop if we've found the control
                if( returnControl != null ) break;
            }

            return (T)returnControl;
        }

        /// <summary>
        /// Retrieves the default value for a type (e.g. 0 for an int, or null for a string).
        /// Extension method version available in Extensions.cs.
        /// </summary>
        /// <typeparam name="T">Type to retrieve the default value for.</typeparam>
        public static T GetDefaultValue<T>()
        {
            // We want an Func<T> which returns the default.
            // Create that expression here.
            Expression<Func<T>> e = Expression.Lambda<Func<T>>(
                // The default value, always get what the *code* tells us.
                Expression.Default( typeof( T ) )
            );

            // Compile and return the value.
            return e.Compile()();
        }

        /// <summary>
        /// Searches for an object property which stores a control's value.
        /// </summary>
        /// <param name="owner">Object to search for properties against.</param>
        /// <param name="propertyNames">Ordered list of property names to search for.</param>
        public static PropertyInfo GetValueProperty( object owner, string[] propertyNames = null )
        {
            // Set default values for the Properties parameter if none have been specified
            string[] properties = propertyNames ?? new[] { "Value", "Checked", "Text" };

            foreach( string propertyName in properties )
            {
                // Attempt to retrieve the property
                PropertyInfo property = owner.GetType().GetProperty( propertyName );

                // Return it if it exists; otherwise, onwards!
                if( property != null ) return property;
            }

            return null;
        }

        /// <summary>
        /// Searches for an Attributes property on an object and returns it, or null if not found.
        /// </summary>
        /// <param name="owner">Object to search for the Attributes property against.</param>
        public static AttributeCollection GetAttributesProperty( object owner )
        {
            PropertyInfo attributesProperty = owner.GetType().GetProperty( "Attributes" );
            if( attributesProperty != null ) return (AttributeCollection)attributesProperty.GetValue( owner );
            else return null;
        }
    }
}