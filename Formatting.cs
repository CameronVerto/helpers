﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Helpers
{
    /// <summary>
    /// Functions for formatting strings.
    /// </summary>
    public static class Formatting
    {
        public static List<Currency> Currencies = new List<Currency>
        {
            new Currency
            {
                Code = "GBP",
                Name = "Pound Sterling",
                Symbol = "£",
                SymbolPosition = Currency.Position.Before
            },
            new Currency
            {
                Code = "USD",
                Name = "US Dollar",
                Symbol = "$",
                SymbolPosition = Currency.Position.Before
            },
            new Currency
            {
                Code = "AUD",
                Name = "Australian Dollar",
                Symbol = "$",
                SymbolPosition = Currency.Position.Before
            },
            new Currency
            {
                Code = "EUR",
                Name = "Euro",
                Symbol = "€",
                SymbolPosition = Currency.Position.Before
            },
            new Currency
            {
                Code = "JPY",
                Name = "Japanese Yen",
                Symbol = "¥",
                SymbolPosition = Currency.Position.Before
            }
        };

        public static Dictionary<string, string> DownloadIcons = new Dictionary<string, string>()
        {
            // Documents
            { "pdf", "fa-file-pdf" },
            { "docx", "fa-file-word" },
            { "doc", "fa-file-word" },
            { "pptx", "fa-file-powerpoint" },
            { "ppt", "fa-file-powerpoint" },
            { "xlsx", "fa-file-excel" },
            { "xls", "fa-file-excel" },

            // Archives
            { "zip", "fa-file-archive" },
            { "rar", "fa-file-archive" },

            // Images
            { "jpg", "fa-file-image" },
            { "jpeg", "fa-file-image" },
            { "gif", "fa-file-image" },
            { "png", "fa-file-image" },
            { "tif", "fa-file-image" },
            { "svg", "fa-file-image" },
            { "eps", "fa-file-image" },

            // Videos
            { "mp4", "fa-file-video" },
            { "wmv", "fa-file-video" },
            { "mov", "fa-file-video" },
            { "avi", "fa-file-video" },
            { "flv", "fa-file-video" },

            // Audio
            { "mp3", "fa-file-audio" },
            { "wav", "fa-file-audio" },
            { "aac", "fa-file-audio" },
            { "flac", "fa-file-audio" },
            { "wma", "fa-file-audio" },
            { "ogg", "fa-file-audio" },

            // Text
            { "txt", "fa-file-alt" },
            { "rtf", "fa-file-alt" }
        };

        /// <summary>
        /// Formats a price for display with an HTML-encoded currency symbol.
        /// </summary>
        /// <param name="input">Price to format for display.</param>
        /// <param name="currencyCode">Three-letter ISO 4217 currency code (e.g. USD) for the <see cref="Currency"/> to format.</param>
        public static string FormatPrice( decimal input, string currencyCode )
        {
            // Retrieve the currency with the specified code
            Currency currency = Currencies.Find( c => c.Code == currencyCode );

            if( currency != null )
            {
                // Place the currency symbol correctly
                if( currency.SymbolPosition == Currency.Position.Before )
                {
                    return currency.SymbolHTML + String.Format( "{0:N2}", input );
                }
                else return String.Format( "{0:N2}", input ) + currency.SymbolHTML;
            }
            else return String.Format( "{0:N2}", input );
        }

        /// <summary>
        /// Performs <see cref="String.Join(string, string[])"/> after removing empty values.
        /// </summary>
        /// <param name="separator">The string to use as a separator. separator is included in the returned string only if values has more than one element.</param>
        /// <param name="values">A collection that contains the objects to concatenate.</param>
        public static string ExcludeJoin( string separator, params string[] values )
        {
            // Store all strings in a list so we can remove them
            List<string> joinLines = new List<string>();
            joinLines.AddRange( values );

            // Check each line to see if it is blank
            foreach ( string joinLine in joinLines.ToArray() )
            {
                // Remove the line if it is blank
                if ( joinLine.IsBlank() ) joinLines.Remove( joinLine );
            }

            return String.Join( separator, joinLines );
        }

        /// <summary>
        /// Performs <see cref="String.Join(string, string[])"/> after removing empty values.
        /// </summary>
        /// <param name="separator">The string to use as a separator. separator is included in the returned string only if values has more than one element.</param>
        /// <param name="values">A collection that contains the objects to concatenate.</param>
        public static string ExcludeJoin( string separator, IEnumerable<string> values )
        {
            return ExcludeJoin( separator, values.ToArray() );
        }

        /// <summary>
        /// Retrieves the Font Awesome icon class for the specified file type (e.g. "fa-file-audio").
        /// </summary>
        /// <param name="filename">Name of the file (including the extension) to retrieve the class for.</param>
        public static string GetFileIconClass( string filename )
        {
            // Retrieve the name of the extension in lowercase
            string extension = Path.GetExtension( filename ).Replace( ".", "" ).ToLower();
            return DownloadIcons.ContainsKey( extension ) ? DownloadIcons[extension] : "fa-file-alt";
        }

        public class Currency
        {
            /// <summary>
            /// Position of a <see cref="Currency"/> symbol relative to the value.
            /// </summary>
            public enum Position { Before, After }

            /// <summary>
            /// Three-letter ISO 4217 currency code (e.g. USD) for this <see cref="Currency"/>.
            /// </summary>
            public string Code { get; set; }

            /// <summary>
            /// Friendly name (e.g. US Dollar) for this <see cref="Currency"/>.
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// The symbol (e.g. £) which represents this <see cref="Currency"/>.
            /// </summary>
            public string Symbol { get; set; }

            /// <summary>
            /// Returns the <see cref="Symbol"/> for this <see cref="Currency"/> escaped for use in HTML.
            /// </summary>
            public string SymbolHTML
            {
                get { return System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode( Symbol, true ); }
            }

            /// <summary>
            /// Position of the <see cref="Symbol"/> relative to the value of this currency.
            /// </summary>
            public Position SymbolPosition { get; set; }
        }
    }
}