﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Helpers
{
    /// <summary>
    /// Functions for performing data imports.
    /// </summary>
    public static class Import
    {
        /// <summary>
        /// Parses a CSV file and returns its contents as a <see cref="DataTable"/>.
        /// The first row of the CSV will determine the <see cref="DataTable"/> columns.
        /// </summary>
        /// <param name="fileContent"><see cref="Stream"/> containing the CSV file data to parse.</param>
        public static DataTable ParseCSV( Stream fileContent )
        {
            DataTable returnTable = new DataTable();

            // Whether we are currently on the first line of the file
            bool isFirstLine = true;

            // Use a StreamReader to parse the file as text
            using( StreamReader reader = new StreamReader( fileContent ) )
            {
                // Check if we have reached the end of the file
                while( reader.Peek() > -1 )
                {
                    string currentLine = reader.ReadLine();

                    if( currentLine.StartsWith( "," ) )
                    {
                        currentLine = "\"\"" + currentLine;
                    }

                    // Split the fields using the delimiter
                    string[] lineData = Regex.Matches( currentLine, @"(?:^|,)(?=[^\""]|(\"")?)\""?((?(1)[^\""]*|[^,\""]*))\""?(?=,|$)" ).Cast<Match>().Select( m => m.Value ).ToArray();

                    for( int i = 0; i < lineData.Length; i++ )
                    {
                        lineData[i] = lineData[i].Trim( ",\"".ToCharArray() ).Trim();
                    }

                    if( isFirstLine )
                    {
                        // Add all fields on the first row as columns
                        foreach( string line in lineData ) returnTable.Columns.Add( line );
                        isFirstLine = false;
                    }
                    else
                    {
                        returnTable.Rows.Add( lineData );
                    }
                }
            }

            return returnTable;
        }
    }
}