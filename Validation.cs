﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Helpers
{
    /// <summary>
    /// Functions for validating and sanitising data.
    /// </summary>
    public static class Validation
    {
        /// <summary>
        /// Removes all HTML tags from the provided string.
        /// </summary>
        /// <param name="input">String to remove HTML tags from.</param>
        public static string RemoveHTML( string input )
        {
            return Regex.Replace( input, "<[^>]*>", "" );
        }

        /// <summary>
        /// Converts the provided string into that which can be used as a file name.
        /// </summary>
        /// <param name="input">String to sanitise.</param>
        public static string SanitiseFileName( string input )
        {
            string invalidChars = Regex.Escape( new string( Path.GetInvalidFileNameChars() ) );
            string invalidRegStr = String.Format( @"([{0}]*\.+$)|([{0}]+)", invalidChars );
            return Regex.Replace( input, invalidRegStr, "_" );
        }

        /// <summary>
        /// Converts the provided string into that which can be used as an SQL parameter name.
        /// </summary>
        /// <param name="input">String to sanitise.</param>
        /// <param name="addBrackets">Whether to wrap the returned string in [square brackets].</param>
        public static string SanitiseSQLName( string input, bool addBrackets = false )
        {
            string returnString = Regex.Replace( input, "([^A-Za-z0-9_])+", "" );

            if ( addBrackets )
            {
                return "[" + returnString + "]";
            }

            return returnString;
        }
    }
}