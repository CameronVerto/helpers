﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Helpers
{
    public static class Crypto
    {
        /// <summary>
        /// Generates a random key for uses such as activation, password reset and email change.
        /// </summary>
        /// <param name="maxSize">Max length of key.</param>
        public static string GetUniqueKey( int maxSize )
        {
            char[] chars = new char[52];
            chars = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];

            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes( data );
            data = new byte[maxSize];
            crypto.GetNonZeroBytes( data );
            StringBuilder result = new StringBuilder( maxSize );

            foreach( byte b in data )
            {
                result.Append( chars[b % ( chars.Length )] );
            }

            return result.ToString();
        }
    }
}