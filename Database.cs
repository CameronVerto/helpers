﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Helpers
{
    /// <summary>
    /// Functions for performing common database operations.
    /// </summary>
    public static class DB
    {
        public class ConnectionStringCollection
        {
            /// <summary>
            /// Returns the connection string with the specified name.
            /// </summary>
            /// <param name="name">Name of the connection string to return.</param>
            public string this[string name]
            {
                get
                {
                    return ConfigurationManager.ConnectionStrings[name].ConnectionString;
                }
            }
        }

        /// <summary>
        /// Collection of available connection strings from the web.config.
        /// </summary>
        public static ConnectionStringCollection ConnectionStrings = new ConnectionStringCollection();

        #region Execution

        /// <summary>
        /// Executes the specified SQL query and returns any result as an object.
        /// </summary>
        /// <param name="query">SQL query to execute.</param>
        /// <param name="parameters">Parameters to include in the query.</param>
        public static object Execute( string query, params SqlParameter[] parameters )
        {
            using( var connection = new SqlConnection( ConnectionStrings["MSSQL"] ) )
            {
                using( var command = new SqlCommand( query, connection ) )
                {
                    connection.Open();

                    if( parameters != null )
                    {
                        command.Parameters.AddRange( parameters );
                    }

                    return command.ExecuteScalar();
                }
            }
        }

        /// <summary>
        /// Executes the specified SQL query and returns any results in a <see cref="DataTable"/>.
        /// </summary>
        /// <param name="query">SQL query to execute.</param>
        /// <param name="parameters">Parameters to include in the query.</param>
        public static DataTable ExecuteToTable( string query, params SqlParameter[] parameters )
        {
            DataTable returnTable = new DataTable();

            using( var connection = new SqlConnection( ConnectionStrings["MSSQL"] ) )
            {
                using( var command = new SqlCommand( query, connection ) )
                {
                    connection.Open();

                    if( parameters != null )
                    {
                        command.Parameters.AddRange( parameters );
                    }

                    using( var adapter = new SqlDataAdapter( command ) )
                    {
                        adapter.Fill( returnTable );
                    }
                }
            }

            return returnTable;
        }

        /// <summary>
        /// Executes the specified SQL query using an existing connection and returns any result as an <see cref="Object"/>.
        /// </summary>
        /// <param name="command">Existing <see cref="SqlCommand"/> with an open connection to use for execution.</param>
        /// <param name="query">SQL query to execute.</param>
        /// <param name="parameters">Parameters to include in the query.</param>
        public static object Execute( SqlCommand command, string query, params SqlParameter[] parameters )
        {
            command.CommandText = query;
            command.Parameters.Clear();

            if( parameters != null )
            {
                command.Parameters.AddRange( parameters );
            }

            return command.ExecuteScalar();
        }

        /// <summary>
        /// Executes the specified SQL query using an existing connection and returns any results in a <see cref="DataTable"/>.
        /// </summary>
        /// <param name="command">Existing <see cref="SqlCommand"/> with an open connection to use for execution.</param>
        /// <param name="query">SQL query to execute.</param>
        /// <param name="parameters">Parameters to include in the query.</param>
        public static DataTable ExecuteToTable( SqlCommand command, string query, params SqlParameter[] parameters )
        {
            DataTable returnTable = new DataTable();

            command.CommandText = query;
            command.Parameters.Clear();

            if( parameters != null )
            {
                command.Parameters.AddRange( parameters );
            }

            using( var adapter = new SqlDataAdapter( command ) )
            {
                adapter.Fill( returnTable );
            }

            return returnTable;
        }

        /// <summary>
        /// Executes the specified SQL query asynchronously and returns any result as an <see cref="Object"/>.
        /// </summary>
        /// <param name="query">SQL query to execute.</param>
        /// <param name="parameters">Parameters to include in the query.</param>
        public static async Task<object> ExecuteAsync( string query, params SqlParameter[] parameters )
        {
            using( var connection = new SqlConnection( ConnectionStrings["MSSQL"] ) )
            {
                using( var command = new SqlCommand( query, connection ) )
                {
                    await connection.OpenAsync();

                    if( parameters != null )
                    {
                        command.Parameters.AddRange( parameters );
                    }

                    return await command.ExecuteScalarAsync();
                }
            }
        }

        /// <summary>
        /// Executes the specified SQL query asynchronously and returns any results in a <see cref="DataTable"/>.
        /// </summary>
        /// <param name="query">SQL query to execute.</param>
        /// <param name="parameters">Parameters to include in the query.</param>
        public static async Task<DataTable> ExecuteToTableAsync( string query, params SqlParameter[] parameters )
        {
            DataTable returnTable = new DataTable();

            using( var connection = new SqlConnection( ConnectionStrings["MSSQL"] ) )
            {
                using( var command = new SqlCommand( query, connection ) )
                {
                    await connection.OpenAsync();

                    if( parameters != null )
                    {
                        command.Parameters.AddRange( parameters );
                    }

                    await Task.Run( () =>
                    {
                        using( var adapter = new SqlDataAdapter( command ) )
                        {
                            adapter.Fill( returnTable );
                        }
                    } );

                    return returnTable;
                }
            }
        }

        #endregion

        /// <summary>
        /// Creates a single SQL connection for multiple SELECT commands.
        /// For bulk update/insert operations, use <see cref="Transact(string, Action{SqlCommand}, string)"/>.
        /// </summary>
        /// <param name="callback">Delegate in which the command's database operations are carried out.</param>
        /// <param name="connectionStringName">Name of the connection string in web.config to use for the command.</param>
        public static void Command( Action<SqlCommand> callback, string connectionStringName = "MSSQL" )
        {
            using( SqlConnection connection = new SqlConnection( ConnectionStrings[connectionStringName] ) )
            {
                connection.Open();

                using( SqlCommand command = new SqlCommand() { Connection = connection } )
                {
                    // Perform the command logic
                    callback.Invoke( command );
                }
            }
        }

        /// <summary>
        /// Creates an SQL transaction and executes the delegated logic before committing and disposing of the connection.
        /// </summary>
        /// <param name="transactionName">Name of the database transaction.</param>
        /// <param name="callback">Delegate in which the transaction's database operations are carried out.</param>
        /// <param name="connectionStringName">Name of the connection string in web.config to use for the transaction.</param>
        public static void Transact( string transactionName, Action<SqlCommand> callback, string connectionStringName = "MSSQL" )
        {
            // Ensure a transaction name has been provided
            if( String.IsNullOrWhiteSpace( transactionName ) ) throw new ArgumentException( "Transaction name may not be blank." );

            using( SqlConnection connection = new SqlConnection( ConnectionStrings[connectionStringName] ) )
            {
                connection.Open();

                SqlCommand command = new SqlCommand();

                // Perform the inserts in a transaction for a hefty speed boost
                using( SqlTransaction transaction = connection.BeginTransaction( transactionName ) )
                {
                    command.Connection = connection;
                    command.Transaction = transaction;

                    // Perform the transaction logic
                    callback.Invoke( command );

                    transaction.Commit();
                }
            }
        }

        /// <summary>
        /// Generates an update statement for the specified table using the provided parameters. Don't forget to add your WHERE clause afterwards!
        /// </summary>
        /// <param name="tableName">Name of the database table to update.</param>
        /// <param name="parameters">Collection of parameters to include in the update statement.</param>
        public static string GetUpdateStatement( string tableName, params SqlParameter[] parameters )
        {
            // Begin the statement with the specified table name
            string returnStatement = "UPDATE [{0}] SET ".FormatSQL( tableName );

            returnStatement = parameters.Aggregate( returnStatement, ( current, parameter ) => current + "[{0}] = @{0}, ".FormatSQL( parameter.ParameterName ) );

            // Remove the trailing comma/space and return the statement
            return returnStatement.TrimEnd( ", ".ToCharArray() );
        }

        /// <summary>
        /// Generates an insert statement for the specified table using the provided parameters.
        /// </summary>
        /// <param name="tableName">Name of the database table to update.</param>
        /// <param name="parameters">Collection of parameters to include in the update statement.</param>
        public static string GetInsertStatement( string tableName, params SqlParameter[] parameters )
        {
            string columnNames = String.Join( ",", parameters.Select( p => "[" + p.ParameterName + "]" ) );
            string paramNames = String.Join( ",", parameters.Select( p => "@" + p.ParameterName ) );
            return String.Format( "INSERT INTO [{0}] ({1}) VALUES ({2})", tableName, columnNames, paramNames );
        }

        /// <summary>
        /// Creates a new instance of an object (or uses one provided) and populates its properties using data from a DataRow.
        /// </summary>
        /// <param name="returnType">Type to return a new instance of.</param>
        /// <param name="data">Data to populate the properties of the returned object.</param>
        /// <param name="returnObject">Existing object instance to populate. Pass null to create a new instance.</param>
        public static object MapDataToObject( Type returnType, DataRow data, object returnObject = null )
        {
            if( returnObject == null )
            {
                // Check if we have a parameterless constructor
                ConstructorInfo constructor = returnType.GetConstructor( BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Type.EmptyTypes, null );

                // Create a new instance of the specified type to return as an object
                returnObject = constructor == null ? Activator.CreateInstance( returnType, BindingFlags.CreateInstance | BindingFlags.Public | BindingFlags.Instance | BindingFlags.OptionalParamBinding, null, new[] { Type.Missing }, CultureInfo.CurrentCulture ) : Activator.CreateInstance( returnType );
            }

            // Loop through columns of the row's parent table
            foreach( DataColumn column in data.Table.Columns )
            {
                // Attempt to retrieve an object property which corresponds with the column name
                PropertyInfo property = returnObject.GetType().GetProperty( column.ColumnName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance );

                // Don't try to set read-only properties
                if( property != null && property.SetMethod != null && property.SetMethod.IsPublic )
                {
                    // Retrive the type from the DataRow and the destination property
                    Type dataType = data[column].GetType();
                    Type destType = property.PropertyType;

                    // Check if the destination property is nullable
                    Type nullableType = Nullable.GetUnderlyingType( property.PropertyType );

                    // Change the destination type to the underlying type for the nullable
                    if( nullableType != null ) destType = nullableType;

                    // See if the property and data type already match
                    if( dataType == destType )
                    {
                        // The types match so we can set the property safely
                        property.SetValue( returnObject, data[column] );
                    }
                    // See if the data type can be converted to the property type
                    else if( data[column] != DBNull.Value && data[column] is IConvertible )
                    {
                        // Attempt to convert the data to the required type and apply it to the return object
                        property.SetValue( returnObject, Convert.ChangeType( data[column], destType ) );
                    }
                }

                // Check for a "Raw" property to store the DataRow
                PropertyInfo rawProperty = returnObject.GetType().GetProperty( "Raw" );

                // Ensure the property is intended for the DataRow type
                if( rawProperty != null && rawProperty.PropertyType == typeof( DataRow ) )
                {
                    rawProperty.SetValue( returnObject, data );
                }
            }

            return returnObject;
        }

        /// <summary>
        /// Creates a new instance of an object and populates its properties using data from a DataRow.
        /// </summary>
        /// <typeparam name="T">Type to return a new instance of.</typeparam>
        /// <param name="data">Data to populate the properties of the returned object.</param>
        /// <param name="returnObject">Existing object instance to populate. Pass null to create a new instance.</param>
        public static T MapDataToObject<T>( DataRow data, object returnObject = null )
        {
            return (T)MapDataToObject( typeof( T ), data, returnObject );
        }

        /// <summary>
        /// Retrieves a single database record and returns it as an instance of the specified type (or null if not found).
        /// </summary>
        /// <param name="returnType">Type to return a new instance of.</param>
        /// <param name="ID">ID (primary key) of database record to return.</param>
        /// <param name="tableName">Name of database table to query.</param>
        /// <param name="idName">Name of primary key column in specified database table.</param>
        public static object GetRecord( Type returnType, object ID, string tableName = "tblContent", string idName = "ID" )
        {
            // Prepare an SQL statement using the provided (or default) table/ID names
            string selectStatement = String.Format( "SELECT TOP 1 * FROM [{0}] WHERE [{1}] = @ID", tableName, idName );

            // Retrieve the data using the provided ID
            DataTable recordData = ExecuteToTable( selectStatement, new SqlParameter( "ID", ID ) );

            // Return an instance of the object if the record is found, otherwise null
            return recordData.Rows.Count > 0 ? MapDataToObject( returnType, recordData.Rows[0] ) : null;
        }

        /// <summary>
        /// Retrieves a single database record and returns it as an instance of the specified type (or null if not found).
        /// </summary>
        /// <typeparam name="T">Type to return a new instance of.</typeparam>
        /// <param name="ID">ID (primary key) of database record to return.</param>
        /// <param name="tableName">Name of database table to query.</param>
        /// <param name="idName">Name of primary key column in the specified database table.</param>
        public static T GetRecord<T>( object ID, string tableName, string idName )
        {
            return (T)GetRecord( typeof( T ), ID, tableName, idName );
        }

        /// <summary>
        /// Retrieves all records from the specified table which match the provided <paramref name="recordIDs"/>, and maps each record to the specified type.
        /// NOTE: ensure that <paramref name="recordIDs"/> are strongly typed (e.g. ints). Do not pass user input as strings as they may be prone to SQL injection.
        /// </summary>
        /// <typeparam name="T">Type to which each returned record should be mapped.</typeparam>
        /// <param name="tableName">Database table to query for records.</param>
        /// <param name="idField">Name of the primary key column for the specified <paramref name="tableName"/>.</param>
        /// <param name="recordIDs">Collection of IDs to search for in the specified <paramref name="idField"/>.</param>
        public static List<T> GetRecords<T>( string tableName = "tblContent", string idField = "ID", params object[] recordIDs )
        {
            // Prepare the SQL query using provided parameters
            string selectStatement = "SELECT * FROM [{0}] WHERE [{1}] IN ({2})";
            selectStatement = String.Format( selectStatement, tableName, idField, String.Join( ",", recordIDs ) );

            // Prepare a new List<> to return
            List<T> returnList = new List<T>();

            // Retrieve the data
            foreach( DataRow record in ExecuteToTable( selectStatement ).Rows )
            {
                returnList.Add( MapDataToObject<T>( record ) );
            }

            return returnList;
        }

        /// <summary>
        /// Executes a stored procedure and returns results as a List<> of instances of the specified type.
        /// </summary>
        /// <param name="ID">ID to pass to stored procedure</param>
        /// <param name="procedureName">Name of database stored procedure to execute.</param>
        /// <param name="idName">Name of primary key column in the specified database table.</param>
        public static List<T> GetRecords<T>( object ID, string procedureName, string idName = "ID" )
        {
            // Prepare an SQL statement using the provided procedure and ID names
            string selectStatement = String.Format( "EXEC [dbo].[{0}] @{1} = @ID", procedureName, idName );

            // Retrieve the data using the provided ID
            DataTable recordData = ExecuteToTable( selectStatement, new SqlParameter( "ID", ID ) );

            // Prepare a new List<> to return
            List<T> returnList = new List<T>();

            foreach( DataRow record in recordData.Rows )
            {
                // Attempt to create a new object instance using the record data and add it to the return list
                T newItem = MapDataToObject<T>( record );
                if( newItem != null ) returnList.Add( newItem );
            }

            return returnList;
        }
    }
}