﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Helpers
{
    /// <summary>
    /// Extension functions for generic types.
    /// </summary>
    public static class Extensions
    {
        #region Collections

        /// <summary>
        /// Returns a delimited list of selected values as a string.
        /// </summary>
        /// <param name="separator">Character to use as a separator between values.</param>
        public static string GetSelectionString( this ListItemCollection items, string separator = "|" )
        {
            return String.Join( separator, items.OfType<ListItem>().Where( i => i.Selected ).Select( i => i.Value ) );
        }

        /// <summary>
        /// Returns a comma-separated list of selected items' names.
        /// </summary>
        /// <param name="useAnd">Whether to use "and" instead of a comma before the last item.</param>
        public static string GetSelectionSummary( this ListItemCollection items, Boolean useAnd = true )
        {
            // Narrow down to just the selected items
            var selectedItems = items.OfType<ListItem>().Where( i => i.Selected ).Select( i => i.Text );

            if( useAnd )
            {
                string output = String.Join( ", ", selectedItems );

                // Find the index of the last comma in the string
                int lastComma = output.LastIndexOf( ", " );

                // Ensure the string contains a comma
                if( lastComma > 0 )
                {
                    // Remove the last comma
                    output = output.Remove( lastComma, 2 );

                    // Insert the ampersand in place of the comma
                    output = output.Insert( lastComma, " & " );
                }
                return output;
            }
            else return String.Join( ", ", selectedItems );
        }

        /// <summary>
        /// Selects items in a <see cref="ListItemCollection"/> based on a list of values.
        /// </summary>
        /// <param name="values">Values to select in the <see cref="ListItemCollection"/>.</param>
        public static void SelectValues( this ListItemCollection items, List<object> values )
        {
            foreach( object value in values )
            {
                // Attempt to find the corresponding item for the value
                var valueItem = items.FindByValue( value.ToString() );

                // Select the value if it exists
                if( valueItem != null ) valueItem.Selected = true;
            }
        }

        #endregion

        #region DataTables

        /// <summary>
        /// Generates an HTML table representation of a <see cref="DataTable"/>.
        /// </summary>
        /// <param name="showHeaders">Whether to display column names in a header row.</param>
        public static HtmlTable ToHTML( this DataTable table, bool showHeaders = true )
        {
            // Build the base table
            HtmlTable returnTable = new HtmlTable()
            {
                Border = 0,
                CellPadding = 0,
                CellSpacing = 0
            };

            if( showHeaders )
            {
                // Build a row for the table headers
                HtmlTableRow headerRow = new HtmlTableRow();

                for( int c = 0; c < table.Columns.Count; c++ )
                {
                    // Create a <th> cell for the header labels
                    HtmlTableCell headerCell = new HtmlTableCell( "th" )
                    { InnerText = table.Columns[c].ColumnName };

                    headerRow.Cells.Add( headerCell );
                }

                returnTable.Rows.Add( headerRow );
            }

            // Build all other rows
            for( int r = 0; r < table.Rows.Count; r++ )
            {
                HtmlTableRow newRow = new HtmlTableRow();

                for( int c = 0; c < table.Columns.Count; c++ )
                {
                    HtmlTableCell newCell = new HtmlTableCell( "td" )
                    { InnerText = table.Rows[r][c].ToString() };

                    newRow.Cells.Add( newCell );
                }

                returnTable.Rows.Add( newRow );
            }

            return returnTable;
        }

        #endregion

        #region HTTP

        /// <summary>
        /// Writes the specified external file directly to an HTTP response output stream, without buffering it in memory.
        /// Note that this method is subject to script timeouts.
        /// </summary>
        /// <param name="fileURL">URL of the file to stream to the client.</param>
        /// <param name="fileName">Name of the file to serve to the client. Leave null to use the file name in the URL.</param>
        public static void TransmitFileExternal( this HttpResponse resp, String fileURL, String fileName = null )
        {
            // Create a stream for the file
            Stream stream = null;

            // This controls how many bytes to read at a time and send to the client
            int bytesToRead = 10000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                // Create a WebRequest to get the file
                WebRequest fileReq = WebRequest.Create( fileURL );

                // Create a response for this request
                WebResponse fileResp = fileReq.GetResponse();

                if( fileReq.ContentLength > 0 )
                {
                    fileResp.ContentLength = fileReq.ContentLength;
                }

                // Get the Stream returned from the response
                stream = fileResp.GetResponseStream();

                // Attempt to retrieve the file name if one wasn't explicitly set
                if( fileName == null ) fileName = Path.GetFileName( fileURL );

                // Clear the response to avoid weird mixed content
                resp.Clear();

                // Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";

                // Name the file 
                resp.AddHeader( "Content-Disposition", "attachment; filename=\"" + fileName + "\"" );
                resp.AddHeader( "Content-Length", fileResp.ContentLength.ToString() );

                int length;
                do
                {
                    // Verify that the client is connected.
                    if( resp.IsClientConnected )
                    {
                        // Read data into the buffer
                        length = stream.Read( buffer, 0, bytesToRead );

                        // Write it out to the response's output stream
                        resp.OutputStream.Write( buffer, 0, length );

                        // Flush the data
                        resp.Flush();

                        // Clear the buffer
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        // Cancel the download if client has disconnected
                        length = -1;
                    }
                } while( length > 0 ); // Repeat until no data is read
            }
            finally
            {
                if( stream != null )
                {
                    // Close the input stream
                    stream.Close();
                }
            }
        }

        #endregion

        #region Objects

        /// <summary>
        /// Extension method version of GetDefaultValue() in Controls.cs.
        /// </summary>
        public static object GetDefaultValue( this Type type )
        {
            // Validate parameters.
            if( type == null ) throw new ArgumentNullException( "Type" );

            // We want an Func<object> which returns the default.
            // Create that expression here.
            Expression<Func<object>> e = Expression.Lambda<Func<object>>(

            // Have to convert to object.
            Expression.Convert(
                // The default value, always get what the *code* tells us.
                Expression.Default( type ), typeof( object )
            ) );

            // Compile and return the value.
            return e.Compile()();
        }

        /// <summary>
        /// Checks whether an object is null or its default value.
        /// </summary>
        public static bool IsNullOrDefault( this object value, bool allowDBNull = false )
        {
            return ( value == null || ( !allowDBNull && value == DBNull.Value ) || value == value.GetType().GetDefaultValue() );
        }

        /// <summary>
        /// Checks whether a RepeaterItem is a standard/alternating item.
        /// </summary>
        public static bool IsStandard( this RepeaterItem item )
        {
            return new[] { ListItemType.Item, ListItemType.AlternatingItem }.Contains( item.ItemType );
        }

        /// <summary>
        /// Shorthand function to perform DataBinder.Eval() on the current RepeaterItem.
        /// </summary>
        public static object Eval( this RepeaterItem item, string expression )
        {
            return DataBinder.Eval( item.DataItem, expression );
        }

        /// <summary>
        /// Retrieves the nearest ancestor of the specified type.
        /// </summary>
        public static T GetAncestor<T>( this Control child )
        {
            return Controls.GetAncestor<T>( child );
        }

        /// <summary>
        /// Retrieves the nearest descendant of the specified type with the specified ID.
        /// </summary>
        /// <param name="ID"><see cref="Control.ID"/> of the <see cref="Control"/> to find. Leave null to return the first instance of the specified type.</param>
        public static T GetDescendant<T>( this Control parent, string ID = null )
        {
            return Controls.GetDescendant<T>( parent, ID );
        }

        /// <summary>
        /// Returns the provided control rendered as a string.
        /// </summary>
        public static string ToHTMLString( this Control control )
        {
            using( StringWriter stringWriter = new StringWriter() )
            {
                using( HtmlTextWriter controlWriter = new HtmlTextWriter( stringWriter ) )
                {
                    control.RenderControl( controlWriter );
                    return stringWriter.ToString();
                }
            }
        }

        /// <summary>
        /// Checks if an object implements <see cref="IConvertible"/>, performs a shorthand call to <see cref="Convert.ChangeType(object, Type)" /> and casts to the desired <see cref="Type"/>.
        /// </summary>
        public static T To<T>( this object value )
        {
            if( value is IConvertible )
            {
                return (T)Convert.ChangeType( value, typeof( T ) );
            }
            else throw new InvalidCastException( "Type of " + typeof( T ).FullName + " does not implement IConvertible and cannot be automatically converted to another system type." );
        }

        #endregion

        #region Strings

        /// <summary>
        /// Returns the specified string if the source string is null or whitespace.
        /// </summary>
        public static string Fallback( this string input, string text )
        {
            return ( String.IsNullOrWhiteSpace( input ) ) ? text : input;
        }

        /// <summary>
        /// Sanitises all provided values before performing String.Format().
        /// </summary>
        public static string FormatSQL( this string input, params string[] values )
        {
            // Prepare a list so we can add items dynamically
            List<string> formatValues = new List<string>();

            foreach( string value in values )
            {
                // Sanitise each item in the list
                formatValues.Add( Validation.SanitiseSQLName( value ) );
            }

            // Format the string using the newly sanitised values
            return String.Format( input, formatValues.ToArray() );
        }

        /// <summary>
        /// Shorthand extension to perform String.IsNullOrWhiteSpace() on a string.
        /// </summary>
        public static bool IsBlank( this string input )
        {
            return String.IsNullOrWhiteSpace( input );
        }

        #endregion
    }
}