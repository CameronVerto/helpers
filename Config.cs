﻿namespace Helpers
{
    /// <summary>
    /// Functions for retrieving and interacting with site configuration.
    /// </summary>
    public static class Config
    {
        /// <summary>
        /// Returns the ID for the default (VERTO) application.
        /// </summary>
        public static string ApplicationID
        {
            get
            {
                if ( _applicationID.IsBlank() )
                {
                    // Query the database for the standard application ID
                    _applicationID = DB.Execute( "SELECT TOP 1 [ApplicationId] FROM [aspnet_Applications] WHERE [ApplicationName] = 'VERTO'" ).ToString();
                }

                return _applicationID;
            }
        }
        private static string _applicationID;
    }
}