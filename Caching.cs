﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;

namespace Helpers
{
    /// <summary>
    /// Functions for quickly and reliably interacting with the system cache.
    /// </summary>
    public static class Caching
    {
        public enum CacheVariation { None, User }

        /// <summary>
        /// Adds an item to the cache with the specified sliding expiration.
        /// </summary>
        /// <param name="key">Unique key of the cache item to add.</param>
        /// <param name="value">Object to store in the cache.</param>
        /// <param name="timeout">Sliding expiration for the cache item.</param>
        /// <param name="varyBy">Context to vary the cache value by.</param>
        public static void AddItem( string key, Object value, TimeSpan timeout, CacheVariation varyBy = CacheVariation.None )
        {
            // Carry out this needlessly long-winded function call
            HttpContext.Current.Cache.Add( GetVariantKey( key, varyBy ), value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, timeout, System.Web.Caching.CacheItemPriority.Default, null );
        }

        /// <summary>
        /// Retrieves a static reference to the specified cache item to avoid race conditions (or returns null if not found).
        /// </summary>
        /// <param name="key">Key of the cache item to return.</param>
        /// <param name="varyBy">Context the cache value was varied by when added to the cache.</param>
        public static object GetItem( string key, CacheVariation varyBy = CacheVariation.None )
        {
            return HttpContext.Current.Cache[GetVariantKey( key, varyBy )];
        }

        /// <summary>
        /// Removes the item with the specified key from the cache.
        /// </summary>
        /// <param name="key">Key of the cache item to remove.</param>
        /// <param name="varyBy">Context the cache value was varied by when added to the cache.</param>
        public static void RemoveItem( string key, CacheVariation varyBy = CacheVariation.None )
        {
            HttpContext.Current.Cache.Remove( GetVariantKey( key, varyBy ) );
        }

        /// <summary>
        /// Retrieves the unique cache key based on the specified key and variation context.
        /// </summary>
        /// <param name="key">Cache key to retrieve the full key for.</param>
        /// <param name="varyBy">Context to vary the cache key with.</param>
        private static string GetVariantKey( string key, CacheVariation varyBy )
        {
            switch( varyBy )
            {
                case CacheVariation.User:

                    // Attempt to retrieve the user ID and append it to the key
                    return key + ( Membership.GetUser().ProviderUserKey.ToString() ?? "" );

                default: return key;
            }
        }

        /// <summary>
        /// Fully clears the application cache.
        /// </summary>
        public static void ClearApplicationCache()
        {
            List<string> keys = new List<string>();

            // Retrieve application cache enumerator
            IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();

            // Copy all keys that currently exist in the cache
            while( enumerator.MoveNext() ) keys.Add( enumerator.Key.ToString() );

            // Delete every key from cache
            for( int i = 0; i < keys.Count; i++ ) HttpContext.Current.Cache.Remove( keys[i] );
        }
    }
}